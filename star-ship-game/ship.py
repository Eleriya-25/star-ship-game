import itertools

from custom_time import sleep
from fire import draw_fire
from utils import draw_frame, read_controls, get_frame_size, update_speed, limit
from utils.frames import rocket_frame_1, rocket_frame_2
from garbage import OBSTACLES
from game_over import draw_game_over_disclaimer
from game_scenario import gun_is_available
import settings


class SpaceShip:
    frames = [rocket_frame_1, rocket_frame_2]

    def __init__(self, start_row, start_column, max_row, max_column):
        self.row, self.column = start_row, start_column
        self.ship_rows, self.ship_columns = self.get_ship_size()
        self.expected_rows_move, self.expected_columns_move = 0, 0

        self.max_row = max_row - self.ship_rows
        self.max_column = max_column - self.ship_columns

        self.current_frame = None
        self._is_destroyed = False
        self.fire_coroutines = []

    async def fly(self, canvas):
        for frame in itertools.cycle(self.frames):
            if self._is_destroyed:
                await draw_game_over_disclaimer(canvas, self.max_row, self.max_column)

            else:
                self.current_frame = frame

                self.check_controls(canvas)
                self.show(canvas)
                self.draw_fire()
                await sleep()

                self.hide(canvas)
                self.check_controls(canvas)
                self.show(canvas)
                self.draw_fire()
                await sleep()

                self.hide(canvas)

    def show(self, canvas):
        draw_frame(canvas, self.row, self.column, self.current_frame)

    def hide(self, canvas):
        draw_frame(canvas, self.row, self.column, self.current_frame, negative=True)

    def check_controls(self, canvas):
        rows_direction, columns_direction, space_pressed = read_controls(canvas)
        self.move(canvas, rows_direction, columns_direction)

        if space_pressed:
            self.fire(canvas)

    def move(self, canvas, rows_direction, columns_direction):
        self.expected_rows_move, self.expected_columns_move = update_speed(self.expected_rows_move, self.expected_columns_move, rows_direction, columns_direction)
        self.row = limit(value=self.row + self.expected_rows_move, min_value=settings.BORDER_INDENT_ROW, max_value=self.max_row)
        self.column = limit(value=self.column + self.expected_columns_move, min_value=settings.BORDER_INDENT_COLUMN, max_value=self.max_column)
        self.check_collision(canvas)

    def check_collision(self, canvas):
        for obstacle in OBSTACLES.copy():
            if obstacle.has_collision(
                obj_corner_row=self.row,
                obj_corner_column=self.column,
                obj_size_rows=self.ship_rows,
                obj_size_columns=self.ship_columns,
            ):
                self.collision(canvas)
                return

    def collision(self, canvas):
        self._is_destroyed = True

    def fire(self, canvas):
        if not gun_is_available():
            return

        new_fire_coroutine = draw_fire(canvas, start_row=self.row, start_column=self.ship_center_column)
        self.fire_coroutines.append(new_fire_coroutine)

    def draw_fire(self):
        for fire_coroutine in self.fire_coroutines.copy():
            try:
                fire_coroutine.send(None)
            except StopIteration:
                self.fire_coroutines.remove(fire_coroutine)

    @property
    def ship_center_column(self):
        return round(self.column + self.ship_columns / 2 - 1)

    def get_ship_size(self):
        rows, columns = 0, 0

        for frame in self.frames:
            frame_rows, frame_columns = get_frame_size(frame)
            rows = max(rows, frame_rows)
            columns = max(columns, frame_columns)

        return rows, columns
