from utils.frames import game_over as game_over_frame
from utils import draw_frame, get_frame_size
from custom_time import sleep


async def draw_game_over_disclaimer(canvas, max_row, max_column):
    frame_row, frame_column = get_frame_size(game_over_frame)

    draw_frame(
        canvas,
        start_row=round((max_row - frame_row) / 2),
        start_column=round((max_column - frame_column) / 2),
        text=game_over_frame,
    )
    await sleep()
