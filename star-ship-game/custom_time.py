import asyncio

TIC_TIMEOUT = 0.1


async def sleep(seconds=TIC_TIMEOUT):
    await asyncio.sleep(seconds)
