from custom_time import sleep

YEAR_DURATION = 1.5
START_YEAR = 1957
GUN_INVENTION_YEAR = 2020
CURRENT_YEAR = None


PHRASES = {
    # Только на английском, Repl.it ломается на кириллице
    1957: "First Sputnik",
    1961: "Gagarin flew!",
    1969: "Armstrong got on the moon!",
    1971: "First orbital space station Salute-1",
    1981: "Flight of the Shuttle Columbia",
    1998: 'ISS start building',
    2011: 'Messenger launch to Mercury',
    2020: "Take the plasma gun! Shoot the garbage!",
}


def _get_garbage_delay_tics(year):
    if year < 1961:
        return None
    elif year < 1969:
        return 20
    elif year < 1981:
        return 14
    elif year < 1995:
        return 10
    elif year < 2010:
        return 8
    elif year < 2020:
        return 6
    else:
        return 2


def get_garbage_delay_tics():
    return _get_garbage_delay_tics(CURRENT_YEAR)


def gun_is_available():
    return CURRENT_YEAR >= GUN_INVENTION_YEAR


async def year_passing(canvas):
    global CURRENT_YEAR

    CURRENT_YEAR = START_YEAR

    while True:
        canvas.clear()
        canvas.border()
        canvas.addstr(1, 1, f"YEAR: {CURRENT_YEAR}.")

        if CURRENT_YEAR in PHRASES:
            canvas.addstr(1, 20, f"This year: {PHRASES[CURRENT_YEAR]}")

        canvas.addstr(1, 70, f"GUN: {'ON' if gun_is_available() else 'OFF'}")

        canvas.refresh()

        await sleep(YEAR_DURATION)

        CURRENT_YEAR += 1
