import asyncio
import curses

from garbage import OBSTACLES
import settings


def fire_reached_obstacle(fire_row, fire_column):
    for obstacle in OBSTACLES.copy():
        if obstacle.has_collision(fire_row, fire_column):
            obstacle.destroyed()

            return True


async def draw_fire(canvas, start_row, start_column, rows_speed=-0.3, columns_speed=0):
    """Display animation of gun shot, direction and speed can be specified."""

    row, column = start_row, start_column

    canvas.addstr(round(row), round(column), '*')
    await asyncio.sleep(0)

    canvas.addstr(round(row), round(column), 'O')
    await asyncio.sleep(0)
    canvas.addstr(round(row), round(column), ' ')

    row += rows_speed
    column += columns_speed

    symbol = '-' if columns_speed else '|'

    rows, columns = canvas.getmaxyx()
    max_row, max_column = rows - settings.BORDER_INDENT_ROW, columns - settings.BORDER_INDENT_COLUMN
    min_row, min_column = settings.BORDER_INDENT_ROW, settings.BORDER_INDENT_COLUMN

    curses.beep()

    while min_row < row < max_row and min_column < column < max_column:
        canvas.addstr(round(row), round(column), symbol)
        await asyncio.sleep(0)
        canvas.addstr(round(row), round(column), ' ')
        row += rows_speed
        column += columns_speed

        if fire_reached_obstacle(row, column):
            return
