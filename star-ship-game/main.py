import asyncio
import curses

from custom_time import sleep
from garbage import draw_garbage
from ship import SpaceShip
from stars import draw_stars
from game_scenario import year_passing
from screen import prepare_window


async def refresh(canvas):
    while True:
        canvas.refresh()
        await sleep()


def draw(canvas):
    loop = asyncio.get_event_loop()

    body, footer = prepare_window(canvas)

    loop.create_task(year_passing(footer))

    lines, cols = body.getmaxyx()
    mid_line = round(lines / 2)
    mid_col = round(cols / 2)

    draw_stars(body, lines, cols)
    loop.create_task(draw_garbage(body, min_column=0, max_column=cols, max_row=lines))

    ship = SpaceShip(start_row=mid_line, start_column=mid_col, max_row=lines, max_column=cols)
    loop.create_task(ship.fly(canvas))

    loop.create_task(refresh(body))
    loop.run_forever()


if __name__ == '__main__':
    curses.update_lines_cols()
    curses.wrapper(draw)
