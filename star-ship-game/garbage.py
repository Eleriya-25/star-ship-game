import random

from custom_time import sleep
from utils.frames import duck, hubble, lamp, trash_small, trash_xl, trash_large
from utils import Obstacle, get_frame_size, draw_frame, explode
from utils.obstacles import _get_bounding_box_lines
from game_scenario import get_garbage_delay_tics
import settings

SPEED = 0.5
FRAMES = [duck, hubble, lamp, trash_small, trash_xl, trash_large]

SHOW_OBSTACLES = False
OBSTACLES = []


class OffScreenException(Exception):
    def __init__(self, obstacle: Obstacle, coroutine=None):
        self.obstacle = obstacle
        self.coroutine = coroutine


class GarbageObstacle(Obstacle):
    def __init__(self, frame, column, max_row):
        self.max_row = max_row
        self.frame = frame
        self.frame_rows, self.frame_columns = get_frame_size(self.frame)
        self._is_destroyed = False

        super().__init__(
            row=0,
            column=column,
            rows_size=self.frame_rows,
            columns_size=self.frame_columns,
        )

    @classmethod
    async def draw(cls, canvas, min_column, max_column, max_row):
        frame = random.choice(FRAMES)
        column = random.randint(min_column, max_column)

        obstacle = cls(column=column, max_row=max_row, frame=frame)
        OBSTACLES.append(obstacle)

        await obstacle.fly(canvas)

    async def fly(self, canvas):
        while True:
            if self._is_destroyed:
                self.boom(canvas)

            self.show(canvas)

            await sleep()

            self.hide(canvas)
            self.move()

    def move(self):
        self.row += SPEED

        if self.row >= self.max_row:
            raise OffScreenException(obstacle=self)

    def destroyed(self):
        self._is_destroyed = True

    def boom(self, canvas):
        center_row, center_column = self.get_center()

        raise OffScreenException(
            obstacle=self,
            coroutine=explode(canvas, center_row, center_column),
        )

    def show(self, canvas):
        draw_frame(canvas, self.row, self.column, self.frame)

        if SHOW_OBSTACLES:
            self.show_obstacle(canvas)

    def hide(self, canvas):
        draw_frame(canvas, self.row, self.column, self.frame, negative=True)

        if SHOW_OBSTACLES:
            self.hide_obstacle(canvas)

    def show_obstacle(self, canvas):
        row, column, frame = self.dump_bounding_box()
        draw_frame(canvas, row, column, frame)

    def hide_obstacle(self, canvas):
        row, column, frame = self.dump_bounding_box()
        draw_frame(canvas, row, column, frame, negative=True)

    def get_bounding_box_corner_pos(self):
        # Переопределяем метод Obstacle, чтобы добиться более подходящей границы
        return self.row, self.column -1

    def get_bounding_box_frame(self):
        # Переопределяем метод Obstacle, чтобы добиться более подходящей границы
        rows, columns = self.rows_size - 1, self.columns_size
        return '\n'.join(_get_bounding_box_lines(rows, columns))

    def get_center(self):
        center_row = self.row + self.frame_rows/2
        center_column = self.column + self.frame_columns / 2
        return center_row, center_column


async def draw_garbage(canvas, min_column, max_column, max_row):
    coroutines = []

    while True:
        timeout = get_garbage_delay_tics()

        if not timeout:
            await sleep()

        else:
            new_coroutine = GarbageObstacle.draw(
                canvas,
                min_column + settings.BORDER_INDENT_COLUMN,
                max_column - settings.BORDER_INDENT_COLUMN,
                max_row - settings.BORDER_INDENT_ROW,
            )
            coroutines.append(new_coroutine)

            for _ in range(timeout):
                for coroutine in coroutines.copy():
                    try:
                        coroutine.send(None)
                    except StopIteration:
                        coroutines.remove(coroutine)
                    except OffScreenException as exc:
                        coroutines.remove(coroutine)
                        OBSTACLES.remove(exc.obstacle)

                        if exc.coroutine:
                            coroutines.append(exc.coroutine)

                await sleep()
