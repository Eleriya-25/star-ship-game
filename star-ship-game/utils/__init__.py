from .controls import read_controls
from .fly_garbage import fly_garbage
from .frame_utils import draw_frame, get_frame_size
from .phisics import update_speed, limit
from .obstacles import Obstacle, show_obstacles, has_collision
from .explosion import explode
