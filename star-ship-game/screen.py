import curses

import settings


def create_canvas_item(canvas, n_rows, n_cols, y_beginning, x_beginning):
    return canvas.derwin(
        n_rows,
        n_cols,
        y_beginning,
        x_beginning,
    )


def prepare_canvas(canvas, draw_border=False):
    canvas.clear()
    curses.curs_set(False)

    if draw_border:
        canvas.border()

    canvas.refresh()
    canvas.nodelay(True)


def prepare_window(canvas):
    prepare_canvas(canvas)
    lines, cols = canvas.getmaxyx()

    body_lines = round(lines * (settings.BODY_HEIGHT_PERCENT / 100))
    table_lines = lines - body_lines

    outer_body_dimensions = {
        "x_beginning": 0,
        "y_beginning": 0,
        "n_rows": body_lines,
        "n_cols": cols,
    }
    inner_body_dimensions = {
        "x_beginning": 0 + settings.BORDER_INDENT_COLUMN,
        "y_beginning": 0 + settings.BORDER_INDENT_ROW,
        "n_rows": body_lines - settings.BORDER_INDENT_ROW * 2,
        "n_cols": cols - settings.BORDER_INDENT_COLUMN * 2,
    }
    footer_dimensions = {
        "x_beginning": 0,
        "y_beginning": body_lines,
        "n_rows": table_lines,
        "n_cols": cols,
    }

    outer_body = create_canvas_item(canvas, **outer_body_dimensions)
    inner_body = create_canvas_item(canvas, **inner_body_dimensions)
    footer = create_canvas_item(canvas, **footer_dimensions)

    prepare_canvas(outer_body, draw_border=True)
    prepare_canvas(inner_body, draw_border=False)
    prepare_canvas(footer, draw_border=True)

    return inner_body, footer
