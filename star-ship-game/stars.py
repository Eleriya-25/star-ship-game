import asyncio
import random
import curses
import itertools
import settings

STARS_NUMBER = 100
BLINKING_POSITIONS = [
    # stile, timeout
    (curses.A_DIM, 2),
    (None, 0.3),
    (curses.A_BOLD, 0.5),
    (None, 0.3),
]


async def blink_random_start(canvas, row, column, symbol):
    start_position = random.randint(0, 4)
    positions = BLINKING_POSITIONS[start_position:] + BLINKING_POSITIONS[:start_position]

    for style, timeout in itertools.cycle(positions):
        if style:
            canvas.addstr(row, column, symbol, style)
        else:
            canvas.addstr(row, column, symbol)
        await asyncio.sleep(timeout)


def draw_star(canvas, lines, cols):
    column = random.randint(settings.BORDER_INDENT_COLUMN, cols - settings.BORDER_INDENT_COLUMN)
    row = random.randint(settings.BORDER_INDENT_ROW, lines - settings.BORDER_INDENT_ROW)
    symbol = random.choice(['+', "*", ".", ":"])
    return blink_random_start(canvas, row, column, symbol)


def draw_stars(canvas, lines, cols):
    loop = asyncio.get_event_loop()

    for _ in range(STARS_NUMBER):
        loop.create_task(draw_star(canvas, lines, cols))
